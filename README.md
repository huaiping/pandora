### Pandora CMS  
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/pandora/blob/master/LICENSE) [![Build Status](https://travis-ci.com/huaiping/pandora.svg?branch=master)](https://travis-ci.com/huaiping/pandora) [![Coverage Status](https://coveralls.io/repos/github/huaiping/pandora/badge.svg?branch=master)](https://coveralls.io/github/huaiping/pandora?branch=master)  
Open Source CMS. Just for practice.

### Requirements
Python 3.5+ [https://www.python.org](https://www.python.org)  
MySQL 5.5+ [https://www.mysql.com](https://www.mysql.com)

### Features
HTML 5 + Django 2.1.5 + Bootstrap 3.4.0

### Installation
```
$ git clone https://github.com/huaiping/pandora.git
$ cd pandora
$ pip3 install -r requirements.txt
$ python3 manage.py makemigrations
$ python3 manage.py migrate
$ python3 manage.py createsuperuser
```

### Demo
[https://www.huaiping.net](https://huaiping.net/v2/)

### License
Licensed under the [MIT License](https://gitlab.com/huaiping/pandora/blob/master/LICENSE).
