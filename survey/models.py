from django.db import models

class Survey(models.Model):
    id = models.AutoField(primary_key = True)
    xh = models.CharField(max_length = 10)
    xm = models.CharField(max_length = 12)
    yw = models.CharField(max_length = 3)
    sx = models.CharField(max_length = 3)
    yy = models.CharField(max_length = 3)
    wl = models.CharField(max_length = 3)
    hx = models.CharField(max_length = 3)
    sw = models.CharField(max_length = 3)
    zz = models.CharField(max_length = 3)
    ls = models.CharField(max_length = 3)
    dl = models.CharField(max_length = 3)
    message = models.CharField(max_length = 300)
    pubtime = models.DateTimeField(auto_now_add = True)
    bj = models.CharField(max_length = 4)
    survey = models.CharField(max_length = 4)
    memo = models.CharField(max_length = 6)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_survey'
    def __str__(self):
        return self.id
