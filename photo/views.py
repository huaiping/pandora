from django.shortcuts import render
from django.db.models import Count
from django.core import serializers
from django.http import HttpResponse
from photo.models import Photo

def index(request):
    categories = Photo.objects.raw('select id, category from swan_photo group by category')
    subcategories = Photo.objects.raw('select * from swan_photo group by subcategory')
    photos = Photo.objects.raw('select * from swan_photo where subcategory=(select subcategory from swan_photo where id=1)')
    count1 = Photo.objects.values('category').annotate(Count('category'))
    return render(request, 'photo_lists.html', {'categories':categories, 'subcategories':subcategories, 'photos':photos})

def view(request, id):
    if id:
        photos = Photo.objects.raw('select * from swan_photo where subcategory=(select subcategory from swan_photo where id=%s)', [id])
    else:
        id = 1
        photos = Photo.objects.raw('select * from swan_photo where subcategory=(select subcategory from swan_photo where id=%s)', [id])
    return render(request, 'photo_items.html', {'photos':photos})

def waterfall(request):
    return render(request, 'photo_waterfall.html')

def loading(request):
    data = serializers.serialize("json", Photo.objects.all(), fields=('url','description'))
    return HttpResponse(data, content_type="application/json")
