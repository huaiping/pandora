from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^view/(\d{1,6})/$', views.view, name='view'),
    url(r'^waterfall/$', views.waterfall, name='waterfall'),
    url(r'^loading/$', views.loading, name='loading'),
]
