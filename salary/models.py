from django.db import models

class Salary(models.Model):
    id = models.AutoField(primary_key = True)
    bh = models.CharField(max_length = 10)
    xm = models.CharField(max_length = 12)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_salary'
    def __str__(self):
        return self.id
