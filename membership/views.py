from django.shortcuts import render
from membership.models import Membership
from django.http import HttpResponseRedirect

def index(request):
    return render(request, 'login.html')

def login(request):
    m = Membership.objects.get(username=request.POST['username'])
    if m.password == request.POST['password']:
        request.session['username'] = m.username
        return HttpResponseRedirect('/membership/plaza/')
    else:
        return render(request, 'login.html')

def register(request):
    if request.method == "POST":
        return render(request, 'login.html')
    else:
        return redirect("/membership/")

def plaza(request):
    return render(request, 'plaza.html')

def logout(request):
    del request.session['username']
    return HttpResponseRedirect('/guestbook/')
