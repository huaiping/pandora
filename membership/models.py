from django.db import models

class Membership(models.Model):
    id = models.AutoField(primary_key = True)
    username = models.CharField(max_length = 255)
    password = models.CharField(max_length = 24)
    email = models.CharField(max_length = 255)
    name = models.CharField(max_length = 24)
    gender = models.CharField(max_length = 6)
    avator = models.CharField(max_length = 255)
    levels = models.CharField(max_length = 1)
    groups = models.CharField(max_length = 24)
    status = models.CharField(max_length = 12)

    class Meta:
        db_table = 'swan_membership'
    def __str__(self):
        return self.id
