from django.db import models

class Video(models.Model):
    id = models.AutoField(primary_key = True)
    title = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255)
    url = models.CharField(max_length = 255)
    author = models.CharField(max_length = 255)
    category = models.CharField(max_length = 255)
    pubtime = models.DateTimeField(auto_now_add = True)
    hits = models.IntegerField(default = 0)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_video'
    def __str__(self):
        return self.id
