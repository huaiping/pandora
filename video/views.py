from django.shortcuts import render
from video.models import Video

def index(request):
    hots = Video.objects.filter(status='passed').order_by('-hits')[:15]
    item = Video.objects.get(id=2)
    pattern = item.url.split('.')[1]
    lectures = Video.objects.filter(category='lecture', status='passed').order_by('-hits')[:15]
    return render(request, 'video.html', {'hots':hots, 'item':item, 'pattern':pattern, 'lectures':lectures})

def view(request,id):
    hots = Video.objects.filter(status='passed').order_by('-hits')[:15]
    if id:
        item = Video.objects.get(id=id)
        pattern = item.url.split('.')[1]
    else:
        id = 1
        item = Video.objects.get(id=id)
        pattern = item.url.split('.')[1]
    lectures = Video.objects.filter(category='lecture', status='passed').order_by('-hits')[:15]
    return render(request, 'video.html', {'hots':hots, 'item':item, 'pattern':pattern, 'lectures':lectures})
