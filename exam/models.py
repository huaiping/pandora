from django.db import models

class Exam(models.Model):
    id = models.AutoField(primary_key = True)
    question = models.CharField(max_length = 255)
    options = models.CharField(max_length = 255)
    answer = models.CharField(max_length = 255)
    pattern = models.CharField(max_length = 24)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_exam'
    def __str__(self):
        return self.id
