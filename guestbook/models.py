from django.db import models

class Guestbook(models.Model):
    id = models.AutoField(primary_key = True)
    title = models.CharField(max_length = 255)
    content = models.CharField(max_length = 3000)
    author = models.CharField(max_length = 255)
    avatar = models.CharField(max_length = 255)
    pubtime = models.DateTimeField(auto_now_add = True)
    reply = models.CharField(max_length = 255)
    modtime = models.DateTimeField(auto_now = True)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_guestbook'
    def __str__(self):
        return self.id
