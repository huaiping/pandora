from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect  
from guestbook.models import Guestbook

def index(request):
    messages = Guestbook.objects.filter(status='passed').all()[:50]
    paginator = Paginator(messages, 10)
    page = request.GET.get('page')
    try:
        pages = paginator.page(page)
    except PageNotAnInteger:
        pages = paginator.page(1)
    except EmptyPage:
        pages = paginator.page(paginator.num_pages)
    return render(request, 'guestbook.html', {'messages':messages, 'pages':pages})

def message(request):
    return HttpResponseRedirect('/membership/')
