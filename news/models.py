from django.db import models

class News(models.Model):
    id = models.AutoField(primary_key = True)
    title = models.CharField(max_length = 255)
    summary = models.CharField(max_length = 300)
    content = models.CharField(max_length = 9000)
    author = models.CharField(max_length = 255)
    category = models.CharField(max_length = 24)
    pubtime = models.DateTimeField(auto_now_add = True)
    modtime = models.DateTimeField(auto_now_add = True)
    hits = models.IntegerField(default = 0)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_news'
    def __str__(self):
        return self.id

class Comment(models.Model):
    id = models.AutoField(primary_key = True)
    sid = models.CharField(max_length = 255)
    content = models.CharField(max_length = 300)
    author = models.CharField(max_length = 255)
    avatar = models.CharField(max_length = 255)
    pubtime = models.DateTimeField(auto_now_add = True)
    status = models.CharField(max_length = 8)

    class Meta:
        db_table = 'swan_comment'
    def __str__(self):
        return self.id
