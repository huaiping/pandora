from django.shortcuts import render
from news.models import News, Comment

def index(request):
    articles = News.objects.filter(status='passed').order_by('-pubtime').all()[:20]
    return render(request, 'news_lists.html', {'articles':articles})

def view(request,id):
    news = News.objects.order_by('-hits')[:15]
    comments = Comment.objects.all()
    if id:
        item = News.objects.get(id=id)
    else:
        id = 1
        item = News.objects.get(id=id)
    return render(request, 'news_items.html', {'item':item, 'comments':comments})

def page(request,id):
    articles = News.objects.filter(status='passed').order_by('-pubtime').all()[:20]
    return render(request, 'news_pages.html', {'articles':articles})
