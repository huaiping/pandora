import os
import sys
import django.core.handlers.wsgi

os.environ['DJANGO_SETTINGS_MODULE'] = 'pandora.settings'
app_path = "/var/www/pandora/"
sys.path.append(app_path)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
