from django.shortcuts import render
from score.models import Score

def index(request):
    return render(request, 'score.html')

def view(request):
    return render(request, 'score.html')
