from django.db import models

class Score(models.Model):
    id = models.AutoField(primary_key = True)
    xh = models.CharField(max_length = 18)
    xm = models.CharField(max_length = 24)
    bj = models.CharField(max_length = 2)
    item = models.CharField(max_length = 60)
    sfzh = models.CharField(max_length = 18)
    bz = models.CharField(max_length = 45)

    class Meta:
        db_table = 'swan_score'
    def __str__(self):
        return self.id
