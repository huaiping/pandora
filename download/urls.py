from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^view/(\d{1,6})/$', views.view, name='view'),
    url(r'^mirrors/$', views.mirrors, name='mirrors'),
]
