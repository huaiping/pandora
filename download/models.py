from django.db import models

class Download(models.Model):
    id = models.AutoField(primary_key = True)
    title = models.CharField(max_length = 255)
    category = models.CharField(max_length = 30)
    description = models.CharField(max_length = 255)
    url = models.CharField(max_length = 255)
    author = models.CharField(max_length = 255)
    pubtime = models.DateTimeField(auto_now_add = True)
    sizes = models.CharField(max_length = 10)
    license = models.CharField(max_length = 24)
    stars = models.CharField(max_length = 15)
    hits = models.IntegerField(default = 0)
    status = models.CharField(max_length = 9)

    class Meta:
        db_table = 'swan_download'
    def __str__(self):
        return self.id
