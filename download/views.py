from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect  
from download.models import Download

def index(request):
    down_hots = Download.objects.filter(status='passed').order_by('-hits')[:15]
    down_lists = Download.objects.filter(status='passed').all().order_by('-pubtime')
    paginator = Paginator(down_lists, 15)
    page = request.GET.get('page')
    try:
        pages = paginator.page(page)
    except PageNotAnInteger:
        pages = paginator.page(1)
    except EmptyPage:
        pages = paginator.page(paginator.num_pages)
    return render(request, 'down_lists.html', {'down_hots':down_hots, 'down_lists':pages})

def view(request,id):
    down_hots = Download.objects.filter(status='passed').order_by('-hits')[:15]
    if id:
        item = Download.objects.get(id=id)
    else:
        id = 1
        item = Download.objects.get(id=id)
    return render(request, 'down_items.html', {'down_hots':down_hots, 'item':item})

def mirrors(request):
    return HttpResponseRedirect('/membership/')
