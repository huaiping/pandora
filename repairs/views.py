from django.shortcuts import render
from repairs.models import Repairs

def index(request):
    repairs_lists = Repairs.objects.order_by('-pubtime')
    return render(request, 'repairs_lists.html', {'repairs_lists':repairs_lists})

def report(request):
    return render(request, 'repairs.html')
