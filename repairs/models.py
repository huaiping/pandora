from django.db import models

class Repairs(models.Model):
    id = models.AutoField(primary_key = True)
    category = models.CharField(max_length = 255)
    department = models.CharField(max_length = 255)
    item = models.CharField(max_length = 255)
    address = models.CharField(max_length = 255)
    details = models.CharField(max_length = 255)
    customer = models.CharField(max_length = 255)
    telephone = models.CharField(max_length = 11)
    pubtime = models.DateTimeField(auto_now_add = True)
    status = models.CharField(max_length = 9)
    assessment = models.CharField(max_length = 255)

    class Meta:
        db_table = 'swan_repairs'
    def __str__(self):
        return self.id
